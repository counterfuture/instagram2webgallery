#!/bin/bash
# sigal, version 2.1.1
# instagram-scraper, version 1.9.1

# CONFIG
account="$3"
INPUTDIR="/tmp/ARCHIVED_IMAGES"
HTMLDIR="/tmp/_HTML"

# CHECK REQUIREMENTS
[ -x "$(command -v instagram-scraper)" ] || { echo "instagram-scraper not found, please install it." ; exit 1 ; }
[ -x "$(command -v sigal)" ] || { echo "sigal not found, please install it." ; exit 1 ; }
[ -x "$(command -v ffmpeg)" ] || { echo "ffmpeg not found, please install it." ; exit 1 ; }

# CHECK SCRIPT DIR
cd "$(dirname "$0")"

# MANUAL
manual() {
echo "Usage: ./`basename $0` <your instagram username> <your instagram password> <TARGET ACCOUNT>"
exit 1
}

# CHECK ARGUMENTS
if [ "$#" -lt 2 ]; then
    manual
fi

# RSYNC ALL PHOTOS AND VIDEOS FROM TARGET ACCOUNT
instagram-scraper $account -u $1 -p $2

# CHECK FOR IF DIRECTORY EXIST
[ ! -d $account ] && {
echo "ERROR!!"
echo "exiting.."
exit 1
}

# CREATE RSYNC BACKUP
tar cvf $account.tar $account

# CREATE TEMPDIR
mkdir TEMP

# MOVE FILES TO TEMPDIR
mv $account TEMP/

# SPLIT FILES
cd TEMP/$account
i=0;
for f in *;
do
    d=ARCHIVE_$(printf %03d $((i/100+1)));
    mkdir -p $d;
    mv "$f" $d;
    let i++;
    echo $d;
done

# CREATE INPUTDIR AND MOVE FILES TO WORKDIR
mkdir -p $INPUTDIR
mv * $INPUTDIR
cd -
rm -rf TEMP/

# CREATE HTML DIR
mkdir -p $HTMLDIR
cd $HTMLDIR

# CREATE SIGAL CONF
cat > sigal.conf.py <<EOT
source = '$INPUTDIR'
theme = 'photoswipe'
use_orig = True
thumb_size = (280, 210)
ignore_directories = []
ignore_files = []
video_converter = 'ffmpeg'

EOT

# CREATE CUSTOM INDEX PAGE
cat > $INPUTDIR/index.md <<EOT
Title: $account Mirror
Archive of [@$account](https://instagram.com/$account)
<br />
Instagram images and videos download by [instagram-scraper](https://github.com/arc298/instagram-scraper)
EOT

sigal build
